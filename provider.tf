terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.26.0"
    }
  }
  backend "gcs" {
    bucket = "tf-state-silver-treat-351915"
    prefix = "terraform/state"
  }
}
provider "google" {
  project = var.gcp_project
  region  = var.gcp_region
}
