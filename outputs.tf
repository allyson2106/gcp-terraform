output "external_ip" {
  description = "External IP Pavlov Server"
  value       = google_compute_instance.pavlov-server.*.network_interface.0.access_config.0.nat_ip

}

output "external_ip_pavlov_server_2" {
  description = "External IP Pavlov Server 2"
  value       = google_compute_instance.pavlov-server-2.*.network_interface.0.access_config.0.nat_ip

}