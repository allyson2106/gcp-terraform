data "cloudinit_config" "conf" {
  gzip          = false
  base64_encode = false

  part {
    content_type = "text/cloud-config"
    content      = file("conf.yaml")
    filename     = "conf.yaml"
  }
}

data "google_secret_manager_secret_version" "ssh" {
  secret = "sshkeys"
  version   = 1
  provider  = google-beta
  project = var.gcp_project
}
resource "google_compute_instance" "pavlov-server-2" {
  can_ip_forward            = false
  deletion_protection       = false
  enable_display            = false
  machine_type              = var.machine_type
  name                      = "pavlov-server-br-2"
  project                   = var.gcp_project
  allow_stopping_for_update = true
  metadata = {
    "ssh-keys"  = "${data.google_secret_manager_secret_version.ssh.secret_data}"
    "user-data" = "${data.cloudinit_config.conf.rendered}"
  }
  tags = [
    "http-server",
    "https-server",
    "pavlov-server",
  ]
  zone = var.gcp_zone

  boot_disk {
    auto_delete = true
    device_name = "pavlov-server-br2"


    initialize_params {
      size  = 30
      type  = "pd-ssd"
      image = var.machine_image
    }
  }



  network_interface {
    network            = "default"
    subnetwork         = "default"
    subnetwork_project = var.gcp_project

    access_config {
      network_tier = "PREMIUM"
    }
  }

  reservation_affinity {
    type = "ANY_RESERVATION"
  }

  scheduling {
    automatic_restart  = false
    min_node_cpus      = 0
    preemptible        = true
    provisioning_model = "SPOT"
  }

  service_account {
    email = var.service_account
    scopes = [
      "cloud-platform",
    ]
  }

  shielded_instance_config {
    enable_integrity_monitoring = true
    enable_secure_boot          = false
    enable_vtpm                 = true
  }

  timeouts {}
}
