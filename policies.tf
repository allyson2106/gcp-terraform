resource "google_compute_firewall" "rules" {
  project     = var.gcp_project
  name        = "pavlov-server-br"
  network     = "default"
  priority    = "0"
  description = "Creates firewall rule targeting tagged instances pavlov-server"

  allow {
    protocol = "tcp"
    ports    = ["80", "8080", "7777", "8177", "9100"]
  }

  source_tags = ["pavlov-server", "http-server", "https-server"]
  target_tags = ["pavlov-server", "http-server", "https-server"]
}
