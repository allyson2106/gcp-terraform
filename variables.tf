# GCP authentication file
variable "gcp_auth_file" {
  type        = string
  description = "GCP authentication file"
  default     = "/home/asm/terraform.json"
}
# define GCP region
variable "gcp_region" {
  type        = string
  description = "GCP region"
  default     = "southamerica-east1"
}
# define GCP zone
variable "gcp_zone" {
  type        = string
  description = "GCP zone"
  default     = "southamerica-east1-a"
}
# define GCP project name
variable "gcp_project" {
  type        = string
  description = "GCP project name"
  default     = "silver-treat-351915"
}

variable "service_account" {
  type        = string
  description = "Service Account Email Default"
  default     = "281242766767-compute@developer.gserviceaccount.com"
}
variable "machine_type" {
  type        = string
  description = "Machine Shape for Pavlov requirements"
  default     = "n2-highcpu-4"

}
variable "machine_image" {
  type        = string
  description = "Machine Image for Pavlov requirements"
  default     = "ubuntu-2004-focal-v20220610"

}