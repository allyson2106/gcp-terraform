resource "google_compute_instance" "pavlov-server" {
  can_ip_forward            = false
  deletion_protection       = false
  enable_display            = false
  machine_type              = "n2-highcpu-4"
  name                      = "pavlov-server-br"
  project                   = var.gcp_project
  allow_stopping_for_update = true
  metadata = {
    "ssh-keys" = file("sshkeys.pub")
  }
  tags = [
    "http-server",
    "https-server",
    "pavlov-server",
  ]
  zone = var.gcp_zone

  boot_disk {
    auto_delete = true
    device_name = "pavlov-server-br"

    initialize_params {
      size = 50
      type = "pd-balanced"
    }
  }

  confidential_instance_config {
    enable_confidential_compute = false
  }

  network_interface {
    network            = "default"
    subnetwork         = "default"
    subnetwork_project = var.gcp_project

    access_config {
      network_tier = "PREMIUM"
    }
  }

  reservation_affinity {
    type = "ANY_RESERVATION"
  }

  scheduling {
    automatic_restart  = false
    min_node_cpus      = 0
    preemptible        = true
    provisioning_model = "SPOT"
  }

  service_account {
    email = var.service_account
    scopes = [
      "cloud-platform",
    ]
  }

  shielded_instance_config {
    enable_integrity_monitoring = true
    enable_secure_boot          = false
    enable_vtpm                 = true
  }

  timeouts {}
}
